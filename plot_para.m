function [] = plot_para(name)
    load(strcat(name, '.mat'));
    
    fontsize = 18;
%         figure;
%         h = bar3(micro_f1);
%         set(gca, 'FontSize', fontsize);
%         set(gca, 'XTickLabel', -5:5);
%         set(gca, 'YTickLabel', -5:0);
%         set(gca, 'YLim', [1 7]);
%     %     set(gca, 'ZTickLabel', 0.3:0.2:0.7);
%     %     set(gca, 'ZLim', [0 0.4]);
%         xlabel('\gamma (log scale)', 'FontSize', fontsize);
%         ylabel('\lambda (log scale)', 'FontSize', fontsize);
%         zlabel('F1-score', 'FontSize', fontsize);
%         ss = strcat(name,'_f1.eps');
%         saveas(gca, ss, 'epsc');
% 
%         figure;
%         h = bar3(auc);
%         set(gca, 'FontSize', fontsize);
%         set(gca, 'XTickLabel', -5:5);
%         set(gca, 'YTickLabel', -5:0);
%         set(gca, 'YLim', [1 7]);
%     %     set(gca, 'ZTickLabel', 0.3:0.2:0.7);
%     %     set(gca, 'ZLim', [0 0.4]);
%         xlabel('\gamma (log scale)', 'FontSize', fontsize);
%         ylabel('\lambda (log scale)', 'FontSize', fontsize);
%         zlabel('AUC', 'FontSize', fontsize);
%         ss = strcat(name,'_auc.eps');
%         saveas(gca, ss, 'epsc');
% 
%         figure;
%         h = bar3(accuracy);
%         set(gca, 'FontSize', fontsize);
%         set(gca, 'XTickLabel', -5:5);
%         set(gca, 'YTickLabel', -5:0);
%         set(gca, 'YLim', [1 7]);
%     %     set(gca, 'ZTickLabel', 0.3:0.2:0.7);
%     %     set(gca, 'ZLim', [0 0.4]);
%         xlabel('\gamma (log scale)', 'FontSize', fontsize);
%         ylabel('\lambda (log scale)', 'FontSize', fontsize);
%         zlabel('Accuracy', 'FontSize', fontsize);
%         ss = strcat(name,'_accuracy.eps');
%         saveas(gca, ss, 'epsc');
        figure;
        h = bar3(mae(:,1:11));
        set(gca, 'FontSize', fontsize);
        set(gca, 'XTickLabel', -5:5);
        set(gca, 'YTickLabel', -5:0);
        set(gca, 'YLim', [1 7]);
    %     set(gca, 'ZTickLabel', 0.3:0.2:0.7);
        set(gca, 'ZLim', [0 1]);
        xlabel('\gamma (log scale)', 'FontSize', fontsize);
        ylabel('\lambda (log scale)', 'FontSize', fontsize);
        zlabel('MAE', 'FontSize', fontsize);
        ss = strcat(name,'_mae.eps');
        saveas(gca, ss, 'epsc');        
        
        figure;
        h = bar3(mse(:,1:11));
        set(gca, 'FontSize', fontsize);
        set(gca, 'XTickLabel', -5:5);
        set(gca, 'YTickLabel', -5:0);
        set(gca, 'YLim', [1 7]);
    %     set(gca, 'ZTickLabel', 0.3:0.2:0.7);
    %     set(gca, 'ZLim', [0 0.4]);
        xlabel('\gamma (log scale)', 'FontSize', fontsize);
        ylabel('\lambda (log scale)', 'FontSize', fontsize);
        zlabel('MSE', 'FontSize', fontsize);
        ss = strcat(name,'_mse.eps');
        saveas(gca, ss, 'epsc');               
end



