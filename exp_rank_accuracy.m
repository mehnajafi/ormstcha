% x = 1:8; y = [0.69,0.76,0.68,0.66,0.69,0.65,0.60,0.62];
% plot(x,y,'*--','linewidth',2);
% xlim([0 10]);
% ylim([0.55 0.8]);
% xlabel('Rank R','fontsize',32); % x-axis label
% ylabel('Accuracy','fontsize',32); % y-axis label
% ax = gca;
% set(ax,'YTick',linspace(0.55,0.8,6),'FontSize',24);
% ti = get(gca,'TightInset');
% set(gca,'Position',[ti(1) ti(2) 1-ti(3)-ti(1) 1-ti(4)-ti(2)]);
% set(gca,'units','centimeters');
% pos = get(gca,'Position');
% ti = get(gca,'TightInset');
% 
% set(gcf, 'PaperUnits','centimeters');
% set(gcf, 'PaperSize', [pos(3)+ti(1)+ti(3) pos(4)+ti(2)+ti(4)]);
% set(gcf, 'PaperPositionMode', 'manual');
% set(gcf, 'PaperPosition',[0 0 pos(3)+ti(1)+ti(3) pos(4)+ti(2)+ti(4)]);
% saveas(gcf,'HIV_rank.pdf');
% 
% x = 0.1*(0:9); y = [0.73,0.76,0.71,0.69,0.69,0.67,0.65,0.68,0.69,0.70];
% %y = fliplr(y);
% plot(x,y,'r*--','linewidth',2);
% xlim([0 1]);
% ylim([0.55 0.8]);
% xlabel('Extraction Rate r','fontsize',32); % x-axis label
% ylabel('Accuracy','fontsize',32); % y-axis label
% ax = gca;
% set(ax,'XTick',linspace(0,1,6),'YTick',linspace(0.55,0.8,6),'FontSize',24);
% saveas(gcf,'HIV_comp.pdf');
% 
% 
% x = 1:8; y = [0.51,0.56,0.52,0.60,0.68,0.64,0.59,0.53];
% plot(x,y,'*--','linewidth',2);
% xlim([0 10]);
% ylim([0.45 0.7]);
% xlabel('Rank R','fontsize',32); % x-axis label
% ylabel('Accuracy','fontsize',32); % y-axis label
% ax = gca;
% set(ax,'YTick',linspace(0.45,0.7,6),'FontSize',24);
% saveas(gcf,'ADHD_rank.pdf');
% 
% x = 0.1*(5:9); y = [0.56,0.68,0.64,0.58,0.56];
% %y = fliplr(y);
% plot(x,y,'r*--','linewidth',2);
% xlim([0.5 1]);
% ylim([0.45 0.7]);
% xlabel('Extraction Rate r','fontsize',32); % x-axis label
% ylabel('Accuracy','fontsize',32); % y-axis label
% ax = gca;
% set(ax,'XTick',linspace(0.5,1,6),'YTick',linspace(0.45,0.7,6),'FontSize',24);
% saveas(gcf,'ADHD_comp.pdf');
% 
% x = 1:8; y = [0.67,0.50,0.58,0.76,0.64,0.60,0.55,0.56];
% plot(x,y,'*--','linewidth',2);
% xlim([0 10]);
% ylim([0.45 0.8]);
% xlabel('Rank R','fontsize',32); % x-axis label
% ylabel('Accuracy','fontsize',32); % y-axis label
% ax = gca;
% set(ax,'YTick',[0.5 0.6 0.7 0.8],'FontSize',24);
% saveas(gcf,'ADNI_rank.pdf');
% 
% 
% x = 0.1*(0:9); y = [0.7,0.76,0.66,0.65,0.59,0.60,0.56,0.62,0.65,0.7];
% %y = fliplr(y);
% plot(x,y,'r*--','linewidth',2);
% xlim([0 1]);
% ylim([0.5 0.8]);
% xlabel('Extraction Rate r','fontsize',32); % x-axis label
% ylabel('Accuracy','fontsize',32); % y-axis label
% ax = gca;
% set(ax,'XTick',linspace(0,1,6),'YTick',linspace(0.5,0.8,4),'FontSize',24);
% saveas(gcf,'ADNI_comp.pdf');

x = 1:8; HIV_y = [0.69,0.76,0.68,0.66,0.69,0.65,0.60,0.62];
ADHD_y = [0.51,0.56,0.52,0.60,0.68,0.64,0.59,0.53];
ADNI_y = [0.67,0.50,0.58,0.76,0.64,0.60,0.55,0.56];
%plot(x,HIV_y,'-ro',x,ADNI_y,'-.b',x,ADHD_y,'s-g','linewidth',2,'MarkerSize',10);
%legend('HIV','ADNI','ADHD');
plot(x,ADNI_y,'-ro',x,HIV_y,'-.b',x,ADHD_y,'s-g','linewidth',2,'MarkerSize',10);
legend('ADNI','HIV','ADHD');
xlim([0 10]);
ylim([0.45 0.8]);
xlabel('Rank R','fontsize',20); % x-axis label
ylabel('Accuracy','fontsize',20); % y-axis label
ax = gca;
set(ax,'YTick',[0.45 0.5 0.6 0.7 0.8],'FontSize',18);
ti = get(gca,'TightInset');
set(gca,'Position',[ti(1) ti(2) 1-ti(3)-ti(1) 1-ti(4)-ti(2)]);
set(gca,'units','centimeters');
pos = get(gca,'Position');
ti = get(gca,'TightInset');

set(gcf, 'PaperUnits','centimeters');
set(gcf, 'PaperSize', [pos(3)+ti(1)+ti(3) pos(4)+ti(2)+ti(4)]);
set(gcf, 'PaperPositionMode', 'manual');
set(gcf, 'PaperPosition',[0 0 pos(3)+ti(1)+ti(3) pos(4)+ti(2)+ti(4)]);
saveas(gcf,'para_rank.pdf');

x = 0.1*(1:10); HIV_y = [0.73,0.76,0.71,0.69,0.69,0.67,0.65,0.68,0.69,0.70];
ADHD_x = 0.1*(1:5); ADHD_y = [0.56,0.68,0.64,0.58,0.56];
ADNI_y = [0.7,0.76,0.66,0.65,0.59,0.60,0.56,0.62,0.65,0.7];
% plot(x,HIV_y,'-ro',x,ADNI_y,'-.b',ADHD_x,ADHD_y,'s-g','linewidth',2,'MarkerSize',10);
% legend('HIV','ADNI','ADHD');
plot(x,ADNI_y,'-ro',x,HIV_y,'-.b',ADHD_x,ADHD_y,'s-g','linewidth',2,'MarkerSize',10);
legend('ADNI','HIV','ADHD');
xlim([0 1]);
ylim([0.55 0.8]);
xlabel('Time Series Extraction Rate ER_t','fontsize',20); % x-axis label
ylabel('Accuracy','fontsize',20); % y-axis label
ax = gca;
set(ax,'XTick',linspace(0,1,6),'YTick',linspace(0.55,0.8,6),'FontSize',18);
saveas(gcf,'para_comp.pdf');
